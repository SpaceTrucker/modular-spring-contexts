package com.gitlab.spacetrucker.modularspringcontexts.testing;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class AnnotationConfigMockedModuleFactoryTest {

	@Configuration("testModule")
	public static class MockModule {

		public static final Object BEAN = new Object();

		@Bean
		public Object bean() {
			return BEAN;
		}
	}

	@Test
	public void shouldCreateApplicationContextWithDeclaredBean() {
		ApplicationContext context = new AnnotationConfigMockedModuleFactory().createTestingModule(MockModule.class);

		Object bean = context.getBean("bean");

		Assert.assertSame(MockModule.BEAN, bean);
	}

	@Test(expected = NoSuchBeanDefinitionException.class)
	public void shouldCreateApplicationContextNotContainingWrongBean() {
		ApplicationContext context = new AnnotationConfigMockedModuleFactory().createTestingModule(MockModule.class);

		context.getBean("wrongBean");
	}

	@Test
	public void shouldCreateApplicationContextWithIdEqualToConfigurationValue() {
		ApplicationContext context = new AnnotationConfigMockedModuleFactory().createTestingModule(MockModule.class);

		Assert.assertEquals("testModule", context.getId());
	}
}
