package com.gitlab.spacetrucker.modularspringcontexts.testing;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class ModuleTestingIntegrationTest {

	@Configuration("serverModule")
	public static class ServerModule {

		private final Object exportedBean = new Object();

		@Bean
		public Object exportedBean() {
			return exportedBean;
		}
	}
	
	@MockedModule
	private ServerModule serverModule;

	@Resource
	private Object beanUnderTest;

	@Before
	public void beforeMethod() {
		ModuleTesting.initIsolatedModule("/clientModule.xml", this);
	}

	@Test
	public void shouldInjectResourceFields()
	{
		Assert.assertNotNull(beanUnderTest);
	}

	@Test
	public void shouldInjectMockedModules() {
		Assert.assertNotNull(serverModule);
	}
}
