package com.gitlab.spacetrucker.modularspringcontexts.testing;

import java.util.Comparator;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class StartupModuleTestingIntegrationTest {

	@Configuration
	public static class ServerModule {

		@Mock
		private Comparator<Integer> comparator;

		@PostConstruct
		public void postConstruction() {
			MockitoAnnotations.initMocks(this);
			Mockito.when(comparator.compare(Mockito.anyInt(), Mockito.anyInt())).thenReturn(0);
		}

		@Bean
		public Comparator<Integer> dependentBean() {
			return comparator;
		}
	}

	public static class StartupBean {
		@Resource
		private Comparator<Integer> dependentBean;

		private int comparisonResult;

		@PostConstruct
		public void postConstruction() {
			comparisonResult = dependentBean.compare(1, 2);
		}

		public int getComparisonResult() {
			return comparisonResult;
		}
	}

	@MockedModule 
	private ServerModule serverModule;
	
	@Resource
	private StartupBean startupBean;

	@Before
	public void beforeMethod() {
		ModuleTesting.initIsolatedModule("/startupTesting.xml", this);
	}

	@Test
	public void shouldHaveUsedDependentBeanDuringStartup() {
		Mockito.verify(serverModule.dependentBean()).compare(1, 2);
		Assert.assertEquals(0, startupBean.getComparisonResult());
	}

}
