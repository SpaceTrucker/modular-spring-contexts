package com.gitlab.spacetrucker.modularspringcontexts.testing;

import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class MockitoModuleTestingIntegrationTest {

	@Configuration("serverModule")
	public static class ServerModule {

		@Mock
		private List<Object> exportedBean;

		@PostConstruct
		public void postConstruction() {
			MockitoAnnotations.initMocks(this);
		}

		@Bean
		public List<Object> exportedBean() {
			return exportedBean;
		}
	}
	
	@MockedModule
	private ServerModule serverModule;

	@Before
	public void beforeMethod() {
		ModuleTesting.initIsolatedModule("/clientModule.xml", this);
	}

	@Test
	public void shouldCreateMocksDuringInit() {
		Assert.assertTrue(Mockito.mockingDetails(serverModule.exportedBean).isMock());
	}

}
