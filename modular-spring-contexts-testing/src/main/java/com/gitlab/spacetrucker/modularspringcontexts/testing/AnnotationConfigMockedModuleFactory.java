package com.gitlab.spacetrucker.modularspringcontexts.testing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * Creates a java based container configuration from the specified class.
 */
public class AnnotationConfigMockedModuleFactory implements MockedModuleFactory {

	@Override
	public ApplicationContext createTestingModule(Class<?> moduleDefinition) {
		final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.getBeanFactory().addBeanPostProcessor(new ExportBeansBeanPostProcessor(context));
		context.register(moduleDefinition);
		Configuration configuration = moduleDefinition.getAnnotation(Configuration.class);
		if (configuration != null && configuration.value() != null) {
			context.setId(configuration.value());
		}
		context.refresh();
		return context;
	}

}
