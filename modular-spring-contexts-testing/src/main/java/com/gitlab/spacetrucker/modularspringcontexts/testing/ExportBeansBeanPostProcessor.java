package com.gitlab.spacetrucker.modularspringcontexts.testing;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gitlab.spacetrucker.modularspringcontexts.module.ModularSpringContextsConstants;

/**
 * Adds the exported meta attribute every bean definition, so that those beans
 * can be imported by the module under test.
 */
public class ExportBeansBeanPostProcessor implements BeanPostProcessor {

	private final AnnotationConfigApplicationContext context;

	ExportBeansBeanPostProcessor(AnnotationConfigApplicationContext context) {
		this.context = context;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		context.getBeanFactory().getMergedBeanDefinition(beanName)
				.setAttribute(ModularSpringContextsConstants.EXPORTED_META_ATTRIBUTE, "true");
		return bean;
	}
}