package com.gitlab.spacetrucker.modularspringcontexts.testing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies that a field on a test class represents a module that is to be
 * mocked. The instance that the annotated field is set to is created via an
 * implementation of {@link MockedModuleFactory}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MockedModule {
}
