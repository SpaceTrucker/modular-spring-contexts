package com.gitlab.spacetrucker.modularspringcontexts.testing;

import org.springframework.context.ApplicationContext;

/**
 * Factory for creating a module based on a definition provided by a java class.
 */
public interface MockedModuleFactory {
	
	/**
	 * @param moduleDefinition
	 *            for which to create an application context
	 * @return the newly created application context
	 */
	ApplicationContext createTestingModule(Class<?> moduleDefinition);

}
