package com.gitlab.spacetrucker.modularspringcontexts.testing;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import com.gitlab.spacetrucker.modularspringcontexts.module.ModularSpringContextsModuleBeanDefinitionParser;

/**
 * Helper class for setting up tests for modules in isolation.
 */
public class ModuleTesting {

	/**
	 * Initializes the module under test from the specified
	 * {@code moduleConfig}. Afterwards {@link MockedModule}s are injected into
	 * the {@code testInstance} and the {@code testInstance} is autowired.
	 * 
	 * @param moduleConfig
	 *            specifies the xml resource from where to load the
	 *            configuration for the module under test
	 * @param testInstance
	 *            that declares dependent modules and executes tests for the
	 *            module under test
	 * @return the newly created isolated module to be tested
	 */
	public static ConfigurableApplicationContext initIsolatedModule(String moduleConfig, Object testInstance) {
		List<Field> moduleFields = findModuleFields(testInstance);
		Map<String, Class<?>> mockModules = getModuleConfigurations(moduleFields);

		ConfigurableApplicationContext module = startIsolatedModule(moduleConfig, mockModules);

		module.getAutowireCapableBeanFactory().autowireBeanProperties(testInstance,
				AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, true);

		injectModules(testInstance, moduleFields, module);

		return module;
	}

	private static void injectModules(Object testInstance, List<Field> moduleFields,
			ConfigurableApplicationContext module) {
		for (Field field : moduleFields) {
			Object bean = module.getBean(field.getName(), ApplicationContext.class).getBean(field.getType());
			field.setAccessible(true);
			try {
				field.set(testInstance, bean);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new IllegalStateException(MessageFormat.format("Could not inject module for field {0}", field),
						e);
			}
		}
	}

	private static Map<String, Class<?>> getModuleConfigurations(List<Field> moduleFields) {
		Map<String, Class<?>> mockModules = new HashMap<String, Class<?>>();
		for (Field field : moduleFields) {
			String fieldName = field.getName();
			if (mockModules.containsKey(fieldName) && !field.getType().equals(mockModules.get(fieldName))) {
				throw new IllegalStateException(MessageFormat
						.format("Multiple modules with same name {0} but different types defined.", fieldName));
			}
			mockModules.put(fieldName, field.getType());
		}
		return mockModules;
	}

	private static List<Field> findModuleFields(Object testInstance) {
		List<Field> moduleFields = new ArrayList<>();
		for (Field field : testInstance.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(MockedModule.class)) {
				moduleFields.add(field);
			}
		}
		return moduleFields;
	}

	private static ConfigurableApplicationContext startIsolatedModule(String moduleConfig,
			Map<String, Class<?>> mockModules) {
		GenericApplicationContext moduleRootContext = new GenericApplicationContext();
		moduleRootContext.setId("moduleRootContext for " + moduleConfig);
		List<String> modules = createAndRegisterMockModules(moduleRootContext, mockModules);
		AbstractBeanDefinition isolatedModuleBeanDefinition = ModularSpringContextsModuleBeanDefinitionParser
				.createBeanDefinition(Collections.singletonList(moduleConfig), modules, null);
		moduleRootContext.registerBeanDefinition("moduleRoot", isolatedModuleBeanDefinition);
		moduleRootContext.refresh();
		ConfigurableApplicationContext moduleRoot = moduleRootContext.getBean("moduleRoot",
				ConfigurableApplicationContext.class);
		return moduleRoot;
	}

	private static List<String> createAndRegisterMockModules(GenericApplicationContext moduleRootContext,
			Map<String, Class<?>> mockModules) {
		List<String> moduleNames = new ArrayList<>();
		AnnotationConfigMockedModuleFactory testingModuleFactory = new AnnotationConfigMockedModuleFactory();
		for (Map.Entry<String, Class<?>> entry : mockModules.entrySet()) {
			ApplicationContext mockModuleContext = testingModuleFactory.createTestingModule(entry.getValue());
			String moduleName = entry.getKey();
			moduleRootContext.getBeanFactory().registerSingleton(moduleName, mockModuleContext);
			moduleNames.add(moduleName);
		}
		return moduleNames;
	}
}
