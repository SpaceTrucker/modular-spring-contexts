package com.gitlab.spacetrucker.modularspringcontexts.singleton;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.beans.factory.xml.AbstractBeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import com.gitlab.spacetrucker.modularspringcontexts.SingletonBeanImportFactoryBean;

/**
 * Bean definition parser for elements like
 * 
 * <pre>
 * &lt;module:import id="importedBean" sourceModule="applicationContext" sourceBean="someBean" /&gt;
 * </pre>
 */
public class ModularSpringContextsSingletonBeanDefinitionParser extends AbstractBeanDefinitionParser {

	@Override
	protected AbstractBeanDefinition parseInternal(Element element, ParserContext parserContext) {
		GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
		beanDefinition.setBeanClass(SingletonBeanImportFactoryBean.class);
		beanDefinition.setPropertyValues(createPropertyValues(element));
		beanDefinition.setScope(ConfigurableBeanFactory.SCOPE_SINGLETON);
		return beanDefinition;
	}

	private MutablePropertyValues createPropertyValues(Element element) {
		String sourceModule = element.getAttribute("sourceModule");
		String sourceBean = element.getAttribute("sourceBean");
		if (sourceBean == null || sourceBean.length() == 0) {
			sourceBean = element.getAttribute("id");
		}
		MutablePropertyValues propertyValues = new MutablePropertyValues();
		propertyValues.addPropertyValue("sourceModuleName", sourceModule);
		propertyValues.addPropertyValue("sourceBeanName", sourceBean);
		return propertyValues;
	}

}
