package com.gitlab.spacetrucker.modularspringcontexts.module;

public final class ModularSpringContextsConstants {

	public static final String EXPORTED_META_ATTRIBUTE = "exported";

	private ModularSpringContextsConstants() {
	}

}
