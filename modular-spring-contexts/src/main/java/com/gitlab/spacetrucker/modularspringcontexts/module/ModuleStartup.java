package com.gitlab.spacetrucker.modularspringcontexts.module;

import java.util.Collections;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

/**
 * Utility class for creating an application context that supports the
 * definition of modules and bean imports.
 */
public final class ModuleStartup {

	private ModuleStartup() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Creates and starts a module based on a bean xml configuration.
	 * <p>
	 * The {@code moduleRootConfig} spring xml context definition is intended to
	 * only contain the definitions of the root modules and their dependencies
	 * amongst each other.
	 * 
	 * @param moduleRootConfig
	 *            from which the root module is created
	 * @return the newly created module
	 */
	public static ConfigurableApplicationContext startModules(String moduleRootConfig) {
		@SuppressWarnings("resource")
		GenericApplicationContext moduleRootContext = new GenericApplicationContext();
		AbstractBeanDefinition moduleRootBeanDefinition = ModularSpringContextsModuleBeanDefinitionParser
				.createBeanDefinition(Collections.singletonList(moduleRootConfig), Collections.<String>emptyList(),
						null);
		moduleRootContext.registerBeanDefinition("moduleRoot", moduleRootBeanDefinition);
		moduleRootContext.refresh();
		ConfigurableApplicationContext moduleRoot = moduleRootContext.getBean("moduleRoot",
				ConfigurableApplicationContext.class);
		moduleRoot.setParent(null);
		return moduleRoot;
	}

}
