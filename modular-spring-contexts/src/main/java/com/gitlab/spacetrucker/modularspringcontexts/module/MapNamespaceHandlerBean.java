package com.gitlab.spacetrucker.modularspringcontexts.module;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * Implementation of {@link NamespaceHandlerSupport} that registers bean
 * definition parsers according to the elements defined in a map.
 */
public class MapNamespaceHandlerBean extends NamespaceHandlerSupport {

	private Map<String, BeanDefinitionParser> elementNameToBeanDefinitionParser;

	@Override
	public void init() {
		for (Entry<String, BeanDefinitionParser> entry : elementNameToBeanDefinitionParser.entrySet()) {
			registerBeanDefinitionParser(entry.getKey(), entry.getValue());
		}
	}

	public void setElementNameToBeanDefinitionParser(
			Map<String, BeanDefinitionParser> elementNameToBeanDefinitionParser) {
		this.elementNameToBeanDefinitionParser = elementNameToBeanDefinitionParser;
		init();
	}

}
