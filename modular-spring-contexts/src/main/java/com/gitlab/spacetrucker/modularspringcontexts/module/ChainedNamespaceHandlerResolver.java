package com.gitlab.spacetrucker.modularspringcontexts.module;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.xml.NamespaceHandler;
import org.springframework.beans.factory.xml.NamespaceHandlerResolver;

/**
 * {@link NamespaceHandlerResolver} implementation that manages a list of
 * {@code NamespaceHandlerResolver}s. The {@link NamespaceHandler} of the first
 * resolver that is able to resolve a given {@code namespaceUri} is chosen.
 */
public class ChainedNamespaceHandlerResolver implements NamespaceHandlerResolver {

	private List<NamespaceHandlerResolver> resolvers = new ArrayList<>();

	@Override
	public NamespaceHandler resolve(String namespaceUri) {
		for (NamespaceHandlerResolver resolver : resolvers) {
			NamespaceHandler resolved = resolver.resolve(namespaceUri);
			if (resolved != null) {
				return resolved;
			}
		}
		return null;
	}

	public void setResolvers(List<NamespaceHandlerResolver> resolvers) {
		this.resolvers = resolvers;
	}

}
