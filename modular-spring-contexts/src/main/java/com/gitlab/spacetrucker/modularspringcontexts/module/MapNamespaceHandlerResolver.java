package com.gitlab.spacetrucker.modularspringcontexts.module;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.xml.NamespaceHandler;
import org.springframework.beans.factory.xml.NamespaceHandlerResolver;

/**
 * Implementation of {@link NamespaceHandlerResolver} that resolves a
 * {@code namespaceUri} according to the values of a predefined {@code Map}.
 */
public class MapNamespaceHandlerResolver implements NamespaceHandlerResolver {

	private Map<String, NamespaceHandler> namespaceUriToHandler = new HashMap<>();

	@Override
	public NamespaceHandler resolve(String namespaceUri) {
		return namespaceUriToHandler.get(namespaceUri);
	}

	public void setNamespaceUriToHandler(Map<String, NamespaceHandler> namespaceUriToHandler) {
		this.namespaceUriToHandler = new HashMap<>(namespaceUriToHandler);
	}

}
