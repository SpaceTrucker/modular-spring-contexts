package com.gitlab.spacetrucker.modularspringcontexts.module;

import java.util.concurrent.Executor;

/**
 * {@link Executor} implementation that immediately runs a command in the
 * current thread.
 */
public class SameThreadExecutor implements Executor {

	@Override
	public void execute(Runnable command) {
		command.run();
	}

}
