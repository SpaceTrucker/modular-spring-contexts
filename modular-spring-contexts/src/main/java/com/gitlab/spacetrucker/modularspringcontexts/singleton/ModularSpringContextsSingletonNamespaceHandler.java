package com.gitlab.spacetrucker.modularspringcontexts.singleton;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class ModularSpringContextsSingletonNamespaceHandler extends NamespaceHandlerSupport {

	public void init() {
		registerBeanDefinitionParser("import", new ModularSpringContextsSingletonBeanDefinitionParser());
	}

}
