package com.gitlab.spacetrucker.modularspringcontexts;

import java.util.concurrent.Executor;

public class TestModuleStarter implements Executor {

	private static ThreadLocal<Boolean> withinExecution = new ThreadLocal<>();
	
	@Override
	public void execute(Runnable command) {
		withinExecution.set(true);
		try {
		command.run();
		} finally {
			withinExecution.set(false);
		}
	}

	public static boolean isWithinExecution() {
		return withinExecution.get() == Boolean.TRUE;
	}
}
