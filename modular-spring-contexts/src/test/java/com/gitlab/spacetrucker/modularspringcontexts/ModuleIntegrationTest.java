package com.gitlab.spacetrucker.modularspringcontexts;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.support.BeanDefinitionValidationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gitlab.spacetrucker.modularspringcontexts.module.ModuleStartup;

public class ModuleIntegrationTest {

	@Test
	public void shouldImportDeclaredSingletonBean() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/singleton-and-prototype/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			Object singleton = client.getBean("importedSingleton");
			Assert.assertEquals(new BigDecimal("123.45"), singleton);
		}
	}

	@Test
	public void shouldImportDeclaredSingletonBeanAsSingleton() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/singleton-and-prototype/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			Assert.assertTrue(client.isSingleton("importedSingleton"));
		}
	}

	@Test
	public void shouldImportDeclaredPrototypeBean() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/singleton-and-prototype/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			Object prototype = client.getBean("importedPrototype");
			Assert.assertEquals(new BigDecimal("543.21"), prototype);
		}
	}

	@Test
	public void shouldImportDeclaredPrototypeBeanAsPrototype() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/singleton-and-prototype/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			Assert.assertTrue(client.isPrototype("importedPrototype"));
		}
	}

	@Test
	public void shouldCreateNewInstancesOfPrototypeUponRequest() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/singleton-and-prototype/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			Object prototype1 = client.getBean("importedPrototype");
			Object prototype2 = client.getBean("importedPrototype");
			Assert.assertNotSame(prototype1, prototype2);
		}
	}

	@Test
	public void shouldImportVisibleBean() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/visibility-visible/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			Object bean = client.getBean("importedBean");
			Assert.assertNotNull(bean);
		}
	}

	@Test
	public void shouldThrowWhenImportingInvisibleBean() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/visibility-invisible/moduleDefinitions.xml")) {
			ApplicationContext client = context.getBean("clientModule", ApplicationContext.class);
			client.getBean("invisibleBean");
			Assert.fail("Bean invisibleBean is visible.");
		} catch (RuntimeException e) {
			Throwable rootCause = getRootCause(e);
			Assert.assertTrue(rootCause instanceof BeanDefinitionValidationException);
		}
	}

	@Test
	public void shouldNotImportBeanFromSiblingNestedModule() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/nested-modules-module-cannot-access-sibling/root-modules.xml")) {
			Assert.fail();
		} catch (BeanCreationException e) {
			Throwable rootCause = e.getRootCause();
			if (rootCause instanceof NoSuchBeanDefinitionException) {
				Assert.assertEquals("nested-1-depth-1", ((NoSuchBeanDefinitionException) rootCause).getBeanName());
			} else {
				throw e;
			}
		}
	}

	@Test
	public void shouldNotSetParentOfModule() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/module-has-no-parent/rootModules.xml")) {
			for (String moduleName : Arrays.asList("rootModuleWithNestedModule", "rootModuleWithoutNestedModule",
					"rootModuleWithDependency")) {
				ApplicationContext module = context.getBean(moduleName, ApplicationContext.class);
				Assert.assertNull("parent is set on module " + moduleName, module.getParent());
			}
		}
	}

	@Test
	public void shouldNotMakeParentModuleAvailableToNestedModule() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/module-has-no-parent/rootModules.xml")) {
			ApplicationContext leafModule = context.getBean("rootModuleWithNestedModule", ApplicationContext.class)
					.getBean("leafModule", ApplicationContext.class);
			Assert.assertFalse(leafModule.containsBean("rootModuleWithNestedModule"));
		}
	}

	@Test
	public void shouldCreateNestedModuleThatDependsUponDependencyOfContainingModule() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/nested-module-may-depend-on-depency-of-parent-module/rootModules.xml")) {
			ApplicationContext leafModule = context.getBean("rootModuleWithNestedModule", ApplicationContext.class)
					.getBean("leafModule", ApplicationContext.class);
			Assert.assertFalse(leafModule.containsBean("rootModuleWithNestedModule"));
		}
	}

	@Test
	public void shouldUseConfiguredModuleStarterForStartingModule() {
		Assert.assertFalse(TestModuleStarter.isWithinExecution());

		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("/configured-module-starter-used/rootModules.xml")) {
			ApplicationContext leafModule = context.getBean("rootModule", ApplicationContext.class);
			Assert.assertNotNull(leafModule);
		}
	}

	@Test
	public void shouldIntegrateWithExistingSpringContexts() {
		try (AbstractXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"/integrate-with-existing-spring-contexts/existingContext.xml")) {
			Object importedBean = context.getBean("importedBean");
			Assert.assertNotNull(importedBean);
		}
	}

	private Throwable getRootCause(RuntimeException e) {
		Throwable current = e;
		while (current.getCause() != null) {
			current = current.getCause();
		}
		return current;
	}

	@Test
	public void shouldImportBeanUsingIdWhenNoSourceBeanSpecified() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("optional-source-bean-dynamic/moduleDefinitions.xml")) {
			Assert.assertNotNull(context.getBean("clientModule", ApplicationContext.class).getBean("someBean"));
		}
	}

	@Test
	public void shouldNotImportBeansFromNonRequiredModule() {

		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("non-dependent-module-import/moduleDefinitions.xml")) {
		} catch (BeanCreationException e) {
			Throwable rootCause = e.getRootCause();
			if (rootCause instanceof NoSuchBeanDefinitionException) {
				Assert.assertEquals("serverModule", ((NoSuchBeanDefinitionException) rootCause).getBeanName());
			} else {
				throw e;
			}
		}
	}

	@Test
	public void foo() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("nested-modules-nested-can-not-access-parent/root-modules.xml")) {
		} catch (BeanCreationException e) {
			Throwable rootCause = e.getRootCause();
			if (rootCause instanceof NoSuchBeanDefinitionException) {
				Assert.assertEquals("nested-1-depth-1", ((NoSuchBeanDefinitionException) rootCause).getBeanName());
			} else {
				throw e;
			}
		}
	}

	@Test
	public void shouldNotMakeRequiredModulesImplicitlyAccessibleForNestedModules() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("dependent-module-not-implicitly-propagated-to-nested-module/root-modules.xml")) {
		} catch (BeanCreationException e) {
			Throwable rootCause = e.getRootCause();
			if (rootCause instanceof NoSuchBeanDefinitionException) {
				Assert.assertEquals("nested-2-depth-1", ((NoSuchBeanDefinitionException) rootCause).getBeanName());
			} else {
				throw e;
			}
		}
	}

	@Test
	public void shouldMakeRequiredModulesExplicitlyAccessibleForNestedModules() {
		try (ConfigurableApplicationContext context = ModuleStartup
				.startModules("dependent-module-not-implicitly-propagated-to-nested-module/root-modules.xml")) {
			context.getBean("nested-1-depth-1", ApplicationContext.class)
					.getBean("nested-1-depth-2", ApplicationContext.class).getBean("importedBean");
		}
	}
}
