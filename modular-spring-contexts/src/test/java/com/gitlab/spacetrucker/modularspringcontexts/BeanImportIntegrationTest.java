package com.gitlab.spacetrucker.modularspringcontexts;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanImportIntegrationTest {

	@Test
	public void shouldImportBean() {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"/rootContext.xml")) {
			Object bean = context.getBean("importedBean");
			Assert.assertEquals(new BigDecimal("123.45"), bean);
		}
	}
	
	@Test
	public void shouldImportBeanAsSingleton() {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"/rootContext.xml")) {
			Assert.assertTrue(context.isSingleton("importedBean"));
		}
	}

	@Test
	public void shouldImportBeanUsingIdWhenNoSourceBeanSpecified() {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"optional-source-bean-static/moduleDefinitions.xml")) {
			Assert.assertNotNull(context.getBean("someBean"));
		}
	}
}
