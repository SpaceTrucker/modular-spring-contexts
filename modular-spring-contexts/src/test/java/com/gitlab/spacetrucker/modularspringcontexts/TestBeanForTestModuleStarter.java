package com.gitlab.spacetrucker.modularspringcontexts;

public class TestBeanForTestModuleStarter {

	public TestBeanForTestModuleStarter() {
		if (!TestModuleStarter.isWithinExecution()) {
			throw new IllegalStateException();
		}
	}

}
